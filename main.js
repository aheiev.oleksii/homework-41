const products = [
    {
        category: 'Phones',
        products: [
            {
                name: 'Iphone 15',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1400,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Samsung Galaxy 30',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1400,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Nokia 1100',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 140,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: 'Computers',
        products: [
            {
                name: 'Mac pro',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 2000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Mac Air',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1400,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'ASUS',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1200,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: "TV's",
        products: [
            {
                name: 'Samsung',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 700,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'LG',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Philips',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1500,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: 'Home appliances',
        products: [
            {
                name: 'Dishwasher',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 500,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Microwave',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 300,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Washing machine',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 700,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: 'Tools',
        products: [
            {
                name: 'Tool set',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 2000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Generator',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 3000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Hammer',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 30,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
];

const navList = document.querySelector('.nav-bar__categories');
const navItems = document.querySelector('.nav-bar__items');
const ul = document.createElement('ul');
const description = document.querySelector('.descr');
const descrWrapp = document.createElement('div');
const form = document.querySelector('.form');
const modal = document.querySelector('.modal');
const closeModalBtn = document.querySelector('.btn-close');
const table = document.querySelector('#order-data');
description.classList.add('d-none');
navItems.classList.add('d-none');
ul.classList.add('nav-bar__list');
let productInfo;

function renderList(category) {
    category.forEach(link => {
        const li = document.createElement('li');
        const a = document.createElement('a');
        li.classList.add('nav-bar__link');

        li.addEventListener('click', e => {
            e.preventDefault();
        });

        li.addEventListener('click', () => {
            navItems.classList.toggle('d-none');
        });

        a.innerText = link.category;
        a.id = link.category;

        link.products.forEach(e => {
            a.href = e.link;
        });

        li.append(a);
        ul.append(li);
    });

    navList.append(ul);
}

renderList(products);

function renderItems(products) {
    products.forEach(card => {
        card.products.forEach(e => {
            const productCard = document.createElement('div');
            const img = document.createElement('img');
            const title = document.createElement('h3');
            const price = document.createElement('h4');
            productCard.classList.add('nav-bar__card');

            productCard.addEventListener('click', () => {
                description.classList.toggle('d-none');
                productInfo = e;
                renderDesrcCard(e);
            });

            productCard.setAttribute('data-filter', `${card.category}`);
            img.src = e.image;
            img.alt = e.name;
            title.innerText = e.name;
            price.innerText = e.price + '$';

            productCard.append(img);
            productCard.append(title);
            productCard.append(price);
            navItems.append(productCard);
        });
    });
}
renderItems(products);

function renderDesrcCard(value) {
    descrWrapp.innerHTML = '';

    const img = document.createElement('img');
    const name = document.createElement('h4');
    const price = document.createElement('h3');
    const descr = document.createElement('p');
    const btn = document.createElement('button');

    btn.addEventListener('click', () => {
        navItems.classList.add('d-none');
        descrWrapp.classList.add('d-none');
        addFormData();
    });

    img.src = value.image;
    img.alt = value.name;
    name.innerText = value.name;
    price.innerText = value.price + '$';
    descr.innerText = value.descr;
    btn.innerText = 'Order';

    descrWrapp.append(img);
    descrWrapp.append(name);
    descrWrapp.append(price);
    descrWrapp.append(descr);
    descrWrapp.append(btn);
    description.prepend(descrWrapp);
}

const filterLinks = document.querySelector('.nav-bar__list');
const galeryCards = document.querySelectorAll('.nav-bar__card');

filterLinks.addEventListener('click', e => {
    if (e.target.id) {
        e.target.classList.add('active');
        description.classList.add('d-none');

        galeryCards.forEach(card => {
            const dataFilter = card.getAttribute('data-filter');

            if (dataFilter === e.target.id) {
                card.classList.remove('hide');
                card.classList.add('show');
            } else {
                card.classList.remove('show');
                card.classList.add('hide');
            }
        });
    }
});

const arr = [false, false, false];

function formValidation() {
    return arr.every((item) => item);
}

function addFormData() {
    form.classList.remove('d-none');
    const firstName = document.querySelector('#surname');
    const secondName = document.querySelector('#secondName');
    const postAddress = document.querySelector('#postAddress');
    const btn = document.querySelector('.button');

    firstName.addEventListener('input', () => {
        if (firstName.value && firstName.value.length > 3) {
            arr[0] = true;
            firstName.style.cssText = 'border: 3px solid rgb(0, 230, 122)';
        } else {
            arr[0] = false;
            firstName.style.cssText = 'border: 3px solid rgb(230, 0, 0)';
            btn.setAttribute('disabled', 'disabled');
        }

        if (formValidation()) btn.removeAttribute('disabled');
    });

    secondName.addEventListener('input', () => {
        if (secondName.value && secondName.value.length > 3) {
            arr[1] = true;
            secondName.style.cssText = 'border: 3px solid rgb(0, 230, 122)';
        } else {
            arr[1] = false;
            secondName.style.cssText = 'border: 3px solid rgb(230, 0, 0)';
            btn.setAttribute('disabled', 'disabled');
        }

        if (formValidation()) btn.removeAttribute('disabled');
    });

    postAddress.addEventListener('input', () => {
        if (postAddress.value && postAddress.value.length > 10) {
            arr[2] = true;
            postAddress.style.cssText = 'border: 3px solid rgb(0, 230, 122)';
        } else {
            arr[2] = false;
            postAddress.style.cssText = 'border: 3px solid rgb(230, 0, 0)';
            btn.setAttribute('disabled', 'disabled');
        }

        if (formValidation()) btn.removeAttribute('disabled');
    });

    formValidation();

    btn.addEventListener('click', () => {
        form.classList.add('d-none');
        openModal();
    });

    form.addEventListener('submit', e => {
        e.preventDefault();
        const dataFromForm = new FormData(e.target);
        const formDataObj = {};
        dataFromForm.forEach((value, key) => (formDataObj[key] = value));
        renderOrderData(formDataObj, productInfo);
    });
}

function openModal() {
    modal.classList.remove('d-none');
}

function closeModal() {
    modal.classList.add('d-none');
    document.location.reload();
}

closeModalBtn.addEventListener('click', closeModal);

function renderOrderData(obj, data) {
    const product = document.createElement('p');
    const price = document.createElement('p');

    product.innerText = data.name;
    price.innerText = data.price + '$';

    table.append(product);
    table.append(price);

    for (let key in obj) {
        const tr = document.createElement('tr');
        const td1 = document.createElement('td');
        const td2 = document.createElement('td');

        td1.innerText = key;
        td2.innerText = obj[key];

        tr.append(td1);
        tr.append(td2);

        table.append(tr);
    }
}